FROM centos:centos7

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && localedef -i ru_RU -f UTF-8 ru_RU.UTF-8

RUN groupadd -r clickhouse --gid=1000; \
	useradd -r -g clickhouse --uid=1000 --home-dir=/etc/clickhouse-server --shell=/bin/bash clickhouse; \
	mkdir -p /etc/clickhouse-server; \
	chown -R clickhouse:clickhouse /etc/clickhouse-server

ENV GOSU_VERSION 1.10
RUN set -x && \
    curl -Lo /usr/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" && \
    curl -Lo /tmp/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" && \
    export GNUPGHOME="$(mktemp -d)" && \
    for server in $(shuf -e ha.pool.sks-keyservers.net \
                            hkp://p80.pool.sks-keyservers.net:80 \
                            keyserver.ubuntu.com \
                            hkp://keyserver.ubuntu.com:80 \
                            pgp.mit.edu) ; do \
        gpg --keyserver "$server" --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && break || : ; \
    done && \
    gpg --batch --verify /tmp/gosu.asc /usr/bin/gosu && \
    rm -r "$GNUPGHOME" /tmp/gosu.asc && \
    chmod +x /usr/bin/gosu && \
    # Allow user to become root
    #chmod u+s /usr/bin/gosu && \
    gosu nobody true

COPY altinity_clickhouse.repo /etc/yum.repos.d/

RUN yum install -y pygpgme yum-utils coreutils epel-release

RUN yum -q makecache -y --disablerepo='*' --enablerepo='altinity_clickhouse' \
	&& yum install -y clickhouse-server

RUN rm /etc/clickhouse-server/config.xml
COPY config.xml /etc/clickhouse-server/config.xml

EXPOSE 8123

VOLUME ["/etc/clickhouse-server", "/var/lib/clickhouse"]

USER clickhouse

CMD clickhouse-server --config-file=/etc/clickhouse-server/config.xml